﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Localization;
using Abp.Runtime.Caching;

namespace MyCompanyName.MyProject.ETag { 
    public class ETagStoreManger : IETagStoreManger
    {
        public const string ETagStoreName = "ETagStoreName";

        private readonly ICacheManager _cacheManager;
        private readonly ILanguageManager _languageManager;

        public ETagStoreManger(ICacheManager cacheManager, ILanguageManager languageManager)
        {
            _cacheManager = cacheManager;
            _languageManager = languageManager;
        }

        public async Task ClearETagAsync()
        {
            await _cacheManager.GetCache<ETagCacheKey, EtagCacheValue>(ETagStoreName).ClearAsync();
        }

        public async Task RemoveETagAsync(ETagCacheKey key)
        {
            await _cacheManager.GetCache<ETagCacheKey, EtagCacheValue>(ETagStoreName).RemoveAsync(key);
        }

        public async Task RemoveUserDefaultETagAsync(int? tenantId, long? userId)
        {
            var culture = _languageManager.CurrentLanguage.Name;
            var languages = _languageManager.GetLanguages();
            foreach (var path in new DefaultEtagPathList())
            {
                foreach (var lang in languages)
                {
                    await RemoveETagAsync(new ETagCacheKey(path, tenantId, userId, lang.Name));
                }
            }
        }

        public async Task<EtagCacheValue> GetETagAsync(ETagCacheKey key, Func<Task<EtagCacheValue>> factory)
        {
            return await _cacheManager.GetCache<ETagCacheKey, EtagCacheValue>(ETagStoreName).GetAsync(key, factory);
        }
    }
}