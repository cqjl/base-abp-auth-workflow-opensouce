﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;

namespace MonitorCenter.Middleware
{
    public static class WebApiEtagMiddlewareExtensions
    {
        public static IApplicationBuilder UseWebApiEtag(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<WebApiEtagMiddleware>();
        }
    }
}
