﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonitorCenter.Middleware
{
    public class ETagEndPointOptions
    {
        public List<string> EndPoints { get; set; }
    }
}
