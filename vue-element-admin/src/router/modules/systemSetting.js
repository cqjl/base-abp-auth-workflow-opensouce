/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const systemSettingRouter = {
  path: '/system-setting',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'SystemSetting',
  meta: {
    title: 'ManageMenu',
    icon: 'setting',
    permission: 'Pages.Administration'
  },
  children: [
    {
      path: 'organization-units',
      name: 'OrganizationUnits',
      component: () =>
        import('@/views/system-setting/organization-units/index'),
      meta: {
        title: 'OrganizationUnits',
        permission: 'Pages.Administration.OrganizationUnits'
      }
    },
    {
      path: 'user',
      name: 'user',
      component: () => import('@/views/system-setting/users/index'),
      meta: { title: 'Users', permission: 'Pages.Administration.Users' }
    },
    {
      path: 'role',
      name: 'role',
      meta: { title: 'Roles', permission: 'Pages.Administration.Roles' },
      component: () => import('@/views/system-setting/role/index')
    },
    {
      path: 'tenant',
      name: 'tenant',
      meta: { title: 'Tenants', permission: 'Pages.Administration.Tenants' },
      component: () => import('@/views/system-setting/tenant/index')
    },
    {
      path: 'flow-schemes',
      name: 'flowSchemes',
      meta: { title: '流程设计' },
      component: () => import('@/views/system-setting/flow-schemes/index')
    },
    {
      path: 'AuditLogs',
      name: 'AuditLogs',
      meta: {
        title: 'AuditLogs',
        permission: 'Pages.Administration.AuditLogs'
      },
      component: () => import('@/views/system-setting/audit-logs/index')
    }
  ]
}

export default systemSettingRouter
