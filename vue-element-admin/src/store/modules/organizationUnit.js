import searchDailog from './common/searchSelectionDialog'
import {
  getOrganizationUnits,
  getOrganizationUnitUsers,
  createOrganizationUnit,
  updateOrganizationUnit,
  moveOrganizationUnit,
  deleteOrganizationUnit,
  removeUserFromOrganizationUnit,
  addUsersToOrganizationUnit,
  findUsers
} from '@/api/organizationUnits'

const organizationUnits = {
  namespaced: true,
  state: {
    userLoading: false, // 控制非dialog界面的searchLoading
    userList: [],
    userTotalCount: 0,
    userPageSize: 10,
    userCurrentPage: 1,

    ...searchDailog.state
  },
  mutations: {
    SET_USER_PAGE_SIZE(state, size) {
      state.searchPageSize = size
    },
    SET_USER_CURRENT_PAGE(state, page) {
      state.searchCurrentPage = page
    },

    ...searchDailog.mutations
  },
  actions: {
    ...searchDailog.actions,

    // 覆盖searchDailog的默认实现
    getSearchSelectionAll({ state }, payload) {
      return new Promise((resolve, reject) => {
        state.searchLoading = true
        findUsers(payload.data)
          .then(response => {
            state.searchList = []
            state.searchList.push(...response.data.result.items)
            state.searchTotalCount = response.data.result.totalCount
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            state.searchLoading = false
          })
      })
    },

    getOrganizationUnits({ state }) {
      return new Promise((resolve, reject) => {
        getOrganizationUnits()
          .then(response => {
            resolve(response.data.result.items)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getOrganizationUnitUsers({ state }, payload) {
      return new Promise((resolve, reject) => {
        state.userLoading = true
        getOrganizationUnitUsers(payload)
          .then(response => {
            state.userList = []
            state.userList.push(...response.data.result.items)
            state.userTotalCount = response.data.result.totalCount
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            state.userLoading = false
          })
      })
    },

    createOrganizationUnit({ state }, payload) {
      return new Promise((resolve, reject) => {
        createOrganizationUnit(payload)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    updateOrganizationUnit({ state }, payload) {
      return new Promise((resolve, reject) => {
        updateOrganizationUnit(payload)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    moveOrganizationUnit({ state }, payload) {
      return new Promise((resolve, reject) => {
        moveOrganizationUnit(payload)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    deleteOrganizationUnit({ state }, payload) {
      return new Promise((resolve, reject) => {
        deleteOrganizationUnit(payload)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    removeUserFromOrganizationUnit({ state }, payload) {
      return new Promise((resolve, reject) => {
        removeUserFromOrganizationUnit(payload)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    addUsersToOrganizationUnit({ state }, payload) {
      return new Promise((resolve, reject) => {
        addUsersToOrganizationUnit(payload)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  }
}

export default organizationUnits
