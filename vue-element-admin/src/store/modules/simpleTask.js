﻿import crud from './common/crudCodeBulid'

const simpleTask = {
  namespaced: true,
  state: {
    ...crud.state
  },
  mutations: {
    ...crud.mutations
  },
  actions: {
    ...crud.actions
  }
}

export default simpleTask
