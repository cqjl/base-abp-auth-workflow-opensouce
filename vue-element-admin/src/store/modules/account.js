import request from '@/utils/request'

const session = {
  namespaced: true,
  state: {},
  actions: {
    async isTenantAvailable({ state }, payload) {
      const rep = await request({
        url: '/api/services/app/Account/IsTenantAvailable',
        method: 'post',
        data: payload.data
      })
      return rep.data.result
    }
  }
}
export default session
