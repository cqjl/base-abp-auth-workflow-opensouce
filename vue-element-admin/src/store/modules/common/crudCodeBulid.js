import request from '@/utils/request'

const state = {
  entityName: '',
  loading: false, // 控制非dialog界面的loading
  list: [],
  totalCount: 0,
  pageSize: 10,
  currentPage: 1
}

const mutations = {
  SET_ENTITY_NAME: (state, entityName) => {
    state.entityName = entityName
  },
  SET_PAGE_SIZE(state, size) {
    state.pageSize = size
  },
  SET_CURRENT_PAGE(state, page) {
    state.currentPage = page
  }
}

const actions = {
  getForEdit({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Get${
          state.entityName
        }ForEdit`,
        method: `get`,
        params: payload.data
      })
        .then(rep => {
          // state.currentUser = {}
          // 下面的items把对象数组展开传入，这里只传入一个
          // state.currentUser = rep.data.result
          var firstLetterlowerName = state.entityName.replace(
            state.entityName[0],
            state.entityName[0].toLowerCase()
          )
          resolve(rep.data.result[firstLetterlowerName])
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  load({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Load`,
        method: `post`,
        data: payload.data
      })
        .then(response => {
          state.list = []
          state.list.push(...response.data.result.items)
          state.totalCount = response.data.result.totalCount
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  getAll({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Get${state.entityName}s`,
        method: `get`,
        params: payload.data
      })
        .then(response => {
          state.list = []
          state.list.push(...response.data.result.items)
          state.totalCount = response.data.result.totalCount
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  deleteItem({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/Delete`,
        method: `delete`,
        params: { id: payload.data.id }
      })
        .then(response => {
          resolve(response.data.result)
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  createOrUpdate({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/CreateOrUpdate${
          state.entityName
        }`,
        method: `post`,
        data: payload.data
      })
        .then(response => {
          resolve()
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  create({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/CreateOrUpdate${
          state.entityName
        }`,
        method: `post`,
        data: payload.data
      })
        .then(response => {
          resolve()
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  },
  update({ state }, payload) {
    return new Promise((resolve, reject) => {
      state.loading = true
      request({
        url: `/api/services/app/${state.entityName}/CreateOrUpdate${
          state.entityName
        }`,
        method: `post`,
        data: payload.data
      })
        .then(response => {
          resolve()
        })
        .catch(error => {
          reject(error)
        })
        .finally(() => {
          state.loading = false
        })
    })
  }
}

export default { state, mutations, actions }
