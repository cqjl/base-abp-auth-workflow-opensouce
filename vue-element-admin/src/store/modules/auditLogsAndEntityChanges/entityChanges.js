import pagedTable from '../common/pagedTable'
import {
  getEntityHistoryObjectTypes,
  getEntityChanges,
  getEntityChangesToExcel,
  getEntityPropertyChanges
} from '@/api/auditlogs'

const entityChanges = {
  namespaced: true,
  state: {
    ...pagedTable.state,

    entityChangesToExcelLoading: false
  },
  mutations: {
    ...pagedTable.mutations
  },
  actions: {
    ...pagedTable.actions,

    // 覆盖默认实现
    getAll({ state }, payload) {
      return new Promise((resolve, reject) => {
        state.loading = true
        getEntityChanges(payload.data)
          .then(response => {
            state.list = []
            state.list.push(...response.data.result.items)
            state.totalCount = response.data.result.totalCount
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            state.loading = false
          })
      })
    },

    getEntityHistoryObjectTypes({ state }) {
      return new Promise((resolve, reject) => {
        getEntityHistoryObjectTypes()
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {})
      })
    },

    getEntityChangesToExcel({ state }, payload) {
      state.entityChangesToExcelLoading = true
      return new Promise((resolve, reject) => {
        getEntityChangesToExcel(payload.data)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
          .finally(() => {
            state.entityChangesToExcelLoading = false
          })
      })
    },

    getEntityPropertyChanges({ state }, payload) {
      return new Promise((resolve, reject) => {
        getEntityPropertyChanges(payload.data)
          .then(response => {
            resolve(response.data.result)
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  }
}

export default entityChanges
