# 工作流系统

## 流程引擎

1.流程模型结构
 
流程模型体现为json格式，而不是定义数据库的节点表、节点流转表等来进行定义，Json的主要节点包括：Node顶点节点、Line节点，邻接表和逆邻接表表示顶点和顶点直接的关系，节点的流转通过解析邻接表上连线判断下一个节点。


2.库表关系

* 工作流模板信息表 FlowScheme
* 工作流流程实例表 FlowInstance, 其中SchemeContent字段保证着当前实例的流程的json模型，对实例的流转判断、处理人获取也是基于改字段的解析
* 工作流实例操作记录 FlowInstanceOperationHistory，显示流程的流转记录
* 工作流实例流转历史记录 FlowInstanceTransitionHistory，主要用于关联具体的表单表、实现数据权限，如：我的请假单、待办请假单、已办请假单

3.表单数据提交，在操作流程实例的时候，获取FlowSchem表的字段FormTypeName，得到表单实体的命名空间，进而使用反射方式(IRepository<T>)提交表单数据

4.流程模板 gooflow

5.根据业务生成流程实例和流程流转历史

6.模型设计器 vue + elementui

7.流程图模型 nodes、lines、formlins、tolines

8.审批人 setInfo

9.条件流转 Compares

10.表单字段控制可操作控制，在表单内根据流程实例的当前节点判断。

11.单号生成功能：“2位标识码”+“10位时间戳”+"3位计数"

12、审批组件：添加单号组件、用户组件、日期组件，使用随机数和watch机制，当用户没此点击“申请”或“审批”时获取新的单号数据、用户数据


### todo:
6.消息提醒


## 自定义表单

1.自定义表单关联流程模型，填写表单时根据模型生成流程实例

2.表单的命名空间为：’MyCompanyName.MyProject.I" + flowInstance.DbName + "AppService‘


## 表单系统

1.库表

## todo：

2.表单设计器

